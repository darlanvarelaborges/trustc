import odoorpc
#
url = 'chocotech.trustcode.com.br'
uid = 'demo'
password = 'demo'
odoo = odoorpc.ODOO(url, port=80)
db_list = odoo.db.list()
db = db_list[5]
odoo.login(db, 'demo', 'demo')

def orderstatus(api):
    cliente_id = api.search('res.partner', [('rg_fisica', '=', '7.217.951')])
    if cliente_id:
        orders = api.search('sale.order', [('partner_id', '=', cliente_id)])
        if orders:
            api.exec_method('sale.order', 'action_confirm', orders)
            print u'ITEM 10 - O STATUS DA ORDEM FOI ALTERADO COM SUCESSO'
if __name__ == '__main__':
	orderstatus()
